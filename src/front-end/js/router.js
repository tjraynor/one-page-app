// import HomePage from './vue/components/routes/home-page.vue';
const HomePage = () => import('./vue/components/routes/home-page.vue');
const ArticlePage = () => import('./vue/components/routes/article-page.vue');
const AboutPage = () => import('./vue/components/routes/about-page.vue');
const NotFoundPage = () => import('./vue/components/routes/404-page.vue');

Vue.use(VueRouter);

export default new VueRouter({
    scrollBehavior() {
        return { x: 0, y: 0 };
    },
    mode: 'history',
    base: '/opa/',
    routes: [
        {
            path: '/',
            name: 'HomePage',
            component: HomePage
        },
        {
            path: '/article/:articleId',
            component: ArticlePage
        },
        {
            path: '/about',
            name: 'AboutPage',
            component: AboutPage
        },
        {
            path: '*',
            name: '404',
            component: NotFoundPage
        }
    ]
});