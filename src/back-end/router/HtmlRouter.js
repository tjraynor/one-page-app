'use strict';

let core        = require('core-libs');
let bootstrap   = core.bootstrap;
let hitch       = core.functions.hitch;
let CoreRouter      = core.router;

let OnePageRenderer    = require('../renderer/OnePageRenderer');

class OnePageRouter extends CoreRouter {
    init() {
        return super.init('opa')
            .then(() => {
                this.start();
                return Promise.resolve();
            });
    }

    start() {
        // Install routes
        this.app
            .get('/*', hitch(OnePageRenderer, 'getPage'))
        ;

    }
}

let _singleton = new OnePageRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;