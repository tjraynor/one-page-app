'use strict';

let path = require('path');

let core        = require('core-libs');
let bootstrap   = core.bootstrap;
let renderEngine    = core.renderEngine;

let OnePageRenderCtx   = require('./context/OnePageRenderContext').factory;

class OnePageRenderer {

    init() {
        this.pages = {
            '/': path.join('home.html')
        };

        return Promise.resolve();
    }

    getPage(req, res, next) {
        if (this.pages.hasOwnProperty(req.url))
        {
            let filePath = renderEngine.getPagePath('opa', this.pages[req.url]);
            res.render(filePath, OnePageRenderCtx.init(req));
        }
        else
        {
            next();
        }
    }
}

let _singleton = new OnePageRenderer();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;